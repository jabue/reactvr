import React from 'react';
import {
  AppRegistry,
  asset,
  Pano,
  Text,
  View,
} from 'react-vr';
import Linking from 'Linking';
export default class PanoView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 0
    };

    // Toggle the state every second
    // setInterval(() => {
    //   var pageValue = this.state.page < 5? this.state.page + 1 : 0;
    //   this.setState({ page: pageValue});
    // }, 1000);
  }

  componentDidMount() {
    // var thisRef = this;
    // var pageValue;
    // var url = Linking.getInitialURL().then((url) => {
    //   if (url) {
    //     var fullurl = new URL(url);
    //     pageValue = fullurl.searchParams.get("page");
    //     console.log(pageValue);
    //     thisRef.setState({page: Number(pageValue)});
    //   }
    // }).catch(err => console.error('An error occurred', err));
  }

  sayHello() {
    console.log('youyou');
  }

  render() {
    var destination = this.state.page + '.jpg';
    return (
      <Pano source={asset(destination)}/>
    );
  }
}
