import React from 'react';
import {
  asset,
  View,
  Image,
} from 'react-vr';
export default class TestImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
  }

  render() {
    return (
      <View>
        <Image style={{layoutOrigin: [3, 3], width: 1, height: 1, transform: [{translate: [0, 0, -6]}]}} source={asset('icon128.png')}/>
      </View>
    );
  }
}
