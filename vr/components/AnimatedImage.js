import React from 'react';
import {
  AppRegistry,
  asset,
  Pano,
  Text,
  View,
  Animated,
} from 'react-vr';
import { Easing } from 'react-native';
export default class AnimatedImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bounceValue: new Animated.Value(0),
    };
  }
  render() {
    return (
      <Animated.Image                         // Base: Image, Text, View
        source={{uri: 'http://i.imgur.com/XMKOH81.jpg'}}
        style={{
          flex: 1,
          width: 1,
          height: 1,
          transform: [                        // `transform` is an ordered array
            {scale: this.state.bounceValue},  // Map `bounceValue` to `scale`
            {translate: [0, 0, -6]},
          ],
          layoutOrigin: [2, 2]
        }}
      />
    );
  }
  componentDidMount() {
    this.state.bounceValue.setValue(1.5);     // Start large
    Animated.timing(                          // Base: spring, decay, timing
      this.state.bounceValue,                 // Animate `bounceValue`
      {
        toValue: 6,                         // Animate to smaller size
        duration: 1000,
        delay: 1000,
        easing: Easing.bounce                          // Bouncier spring
      }
    ).start();                                // Start the animation
  }
}
