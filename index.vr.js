import React from 'react';
import {
  AppRegistry,
  asset,
  Pano,
  Text,
  View,
} from 'react-vr';
import Linking from 'Linking';
import PanoView from './vr/components/PanoView.js';
import TestImage from './vr/components/TestImage.js';

export default class reactvrproject extends React.Component {
  render() {
    return (
      <View>
        <PanoView />
        <TestImage />
      </View>
    );
  }
};

AppRegistry.registerComponent('reactvrproject', () => reactvrproject);
